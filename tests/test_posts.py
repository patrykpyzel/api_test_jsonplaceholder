import json
from unittest.mock import patch
from nose.tools import assert_is_not_none, assert_list_equal, assert_equal, assert_dict_equal
from services import get_comments_under_the_post, get_comment, add_post, edit_post, get_post, delete_post


def test_loading_comments_under_the_post():
    actual = get_comments_under_the_post(13)
    assert_equal(200, actual.status_code)
    assert_is_not_none(actual)
    actual_values = actual.json()
    actual_keys = actual_values[0].keys()

    with patch('requests.get') as mock_get:
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = [{
            'postId': 1,
            'id': 1,
            'name': 'name',
            'email': 'email@email.com',
            'body': 'body'
        }]

        mocked = get_comments_under_the_post(13)
        mocked_values = mocked.json()
        mocked_keys = mocked_values[0].keys()

    assert_list_equal(list(actual_keys), list(mocked_keys))
    assert_equal(5, len(list(actual_values)))


def test_loading_comment():
    actual = get_comment(61)
    actual_values = actual.json()
    actual_keys = actual_values[0].keys()

    with patch('requests.get') as mock_get:
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = [{
            'postId': 1,
            'id': 1,
            'name': 'name',
            'email': 'email@email.com',
            'body': 'body'
        }]

        mocked = get_comment(61)
        mocked_values = mocked.json()
        mocked_keys = mocked_values[0].keys()

    assert_list_equal(list(actual_keys), list(mocked_keys))

    for k, v in actual_values[0].items():
        assert_is_not_none(actual_values[0][k])


def test_adding_post():
    new_post_data = {
        'title': 'foo',
        'body': 'bar',
        'userId': 1
    }

    new_post = add_post(new_post_data)
    assert_equal(201, new_post.status_code)
    added_post = json.loads(new_post.text)

    with patch('requests.get') as mock_get:
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = [{
                "id": added_post['id'],
                "title": "foo",
                "body": "bar",
                "userId": "1"
        }]
        mocked = get_post(added_post['id'])
        mocked_values = mocked.json()

    assert_dict_equal(added_post, mocked_values[0])


def test_editing_post():
    edited_post_data = {
        'id': 1,
        'title': 'foo',
        'body': 'bar',
        'userId': 1
    }

    edited_post = edit_post(edited_post_data)
    assert_equal(200, edited_post.status_code)
    added_post = json.loads(edited_post.text)

    with patch('requests.get') as mock_get:
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = [{
                "id": 1,
                "title": "foo",
                "body": "bar",
                "userId": "1"
        }]
        mocked = get_post(1)
        mocked_values = mocked.json()

    assert_dict_equal(added_post, mocked_values[0])


def test_deleting_post():
    post_id = 1
    deleted_post = delete_post(post_id)
    assert_equal(200, deleted_post.status_code)

    with patch('requests.get') as mock_get:
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = [dict()]
        mocked = get_post(post_id)
        mocked_values = mocked.json()

    assert_dict_equal(dict(), mocked_values[0])

