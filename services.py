from urllib.parse import urljoin
import requests
from constants import BASE_URL

POSTS_URL = urljoin(BASE_URL, 'posts')


def delete_post(post_id):
    response = requests.delete(POSTS_URL+'/{}'.format(post_id))
    if response:
        if response.ok:
            return response
        else:
            return None
    else:
        return None


def get_post(post_id):
    response = requests.get(POSTS_URL+'/{}'.format(post_id))
    if response:
        if response.ok:
            return response
        else:
            return None
    else:
        return None


def edit_post(post):
    response = requests.put(POSTS_URL+'/{}'.format(post['id']), data=post)
    if response:
        if response.ok:
            return response
        else:
            return None
    else:
        return None


def add_post(new_post):
    response = requests.post(POSTS_URL, data=new_post)
    if response:
        if response.ok:
            return response
        else:
            return None
    else:
        return None
    

def get_comment(comment_id):
    comment_url = urljoin(BASE_URL, 'comments?id={}'.format(comment_id))
    response = requests.get(comment_url)
    if response:
        if response.ok:
            return response
        else:
            return None
    else:
        return None


def get_comments_under_the_post(post_id):
    if post_id:
        post_comments_url = urljoin(BASE_URL, 'comments?postId={}'.format(post_id))
    else:
        post_comments_url = urljoin(BASE_URL, 'comments')

    response = requests.get(post_comments_url)
    if response:
        if response.ok:
            return response
        else:
            return None
    else:
        return None
