from tests import test_posts

if __name__ == "__main__":
    test_posts.test_loading_comments_under_the_post()
    test_posts.test_loading_comment()
    test_posts.test_adding_post()
    test_posts.test_editing_post()
    test_posts.test_deleting_post()
